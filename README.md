# 1000x Backend
Backend for the 1000x.ch page.

## Requiremnts

* Flask
* Flask-RESTful
* Flask-JWT-Extended
* Flask-Migrate
* Flask-SQLAlchemy
* Flask-CORS
* SQLAlchemy-serializer
* web3
* apscheduler
* mysqlclient

## Setup

### MySQL
```
CREATE DATABASE [database];

CREATE USER '[username]'@'localhost' IDENTIFIED WITH authentication_plugin BY '[password]';

SELECT user FROM mysql.user;

GRANT ALL ON [database].* TO '[username]'@'localhost';

FLUSH PRIVILEGES;
```

### Setup Application
```
#Debian only
sudo apt install default-libmysqlclient-dev

#Fedora
sudo dnf install python python-devel mysql-devel redhat-rpm-config gcc

cd [project_folder]
python3 -m venv venv
source venv/bin/activate

#update pip
pip install -U pip

pip3 install -r requirements.txt
```

### Freeze
```
pip freeze > requirements.txt
```

## Start
```
# In oder to start de vitual emviremend.
source venv/bin/activate

# In oder to start flask
flask --debug run
```

## Initializ the database
```
flask db init
flask db migrate -m "Initial migration."
flask db upgrade
```

## Update the database
```
flask db migrate -m "Changed User attribute username to name."
flask db upgrade
```

## User CLI
```

#User
flask user --help 													#Help page
flask user add [-n|--name <name>] [-e|--email <email>]				#New user
flask user rm [-id <id>] [-n|--name <name>] [-e|--email <email>]	#Remove user
flask user list 													#List users

```


## Run with a Production Server
When running publicly rather than in development, you should not use the built-in development server (flask run). The development server is provided by Werkzeug for convenience, but is not designed to be particularly efficient, stable, or secure.

Instead, use a production WSGI server. For example, to use Waitress, first install it in the virtual environment:

```
pip install waitress
```

You need to tell Waitress about your application, but it doesn’t use --app like flask run does. You need to tell it to import and call the application factory to get an application object.

```
waitress-serve --call 'app:create_app'
```

## API Documentation
You can open the API documentation under the link: `http://localhost:5000/apidocs/`

## License
Copyright © 2022 Musdasch <musdasch@protonmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.