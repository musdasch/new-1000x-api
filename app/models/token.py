from flask_restful import reqparse
from sqlalchemy_serializer import SerializerMixin


from app.models import db
from app.doc import swagger


parser = reqparse.RequestParser()

parser.add_argument(
	'name',
	type=str,
	help='Name of the token.'
)

parser.add_argument(
	'symbol',
	type=str,
	help='Symbol of the token.'
)

parser.add_argument(
	'standard',
	type=str,
	help='Standard of the token.(ERC20, ERC721)'
)

parser.add_argument(
	'address',
	type=str,
	help='Contract address of the token.'
)

parser.add_argument(
	'decimals',
	type=int,
	help='Decimals places of the token.'
)

parser.add_argument(
	'image',
	type=str,
	help='The token icon.'
)


parser.add_argument(
	'hidden',
	type=bool,
	help='Hide on the swap page.'
)

parser.add_argument(
	'blockchainId',
	type=int,
	help='ID of the Blockchain.'
)

@swagger.definition('Token')
class Token(db.Model, SerializerMixin):
	
	# Swagger
	"""
	Token Object
	---
	type: object
	properties:
		id:
			type: integer
			description: ID of the token.
		name:
			type: string
			description: Name of the token.
		symbol:
			type: string
			description: Token ticker.
		standard:
			type: string
			description: Standard of the token like ERC20 or ERC721.
		address:
			type: string
			description: Address of the token contract.
		decimals:
			type: integer
			description: decimal places of the token.
		image:
			type: string
			description: URL of the token icon.
		hidden:
			type: bool
			description: Should the token be hidden on the ThousandX swap.
		blockchainId:
			type: bool
			description: Local blockchain ID.
	"""

	serialize_only = (
		'id',
		'name',
		'symbol',
		'standard',
		'address',
		'decimals',
		'image',
		'hidden',
		'blockchainId'
	)

	id = db.Column(
		db.Integer,
		primary_key=True
	)

	name = db.Column(
		db.String(255),
		unique=True,
		nullable=False
	)

	symbol = db.Column(
		db.String(15),
		unique=True,
		nullable=False
	)

	standard = db.Column(
		db.String(15),
		nullable=False
	)

	address = db.Column(
		db.String(42),
		unique=True,
		nullable=False
	)

	decimals = db.Column(
		db.Integer,
		nullable=False
	)

	image = db.Column(
		db.String(255),
		nullable=False
	)

	hidden = db.Column(
		db.Boolean,
		nullable=False,
		default=False
	)

	blockchainId = db.Column(
		db.Integer,
		db.ForeignKey('blockchain.id'),
		nullable=False
	)

	def __repr__(self):
		return "<Token %r>" % (self.id)

	def __init__(self, args=None, **kwargs):
		if args==None:
			super(Token, self).__init__(**kwargs)
		else:
			super(Token, self).__init__(
				blockchainId	= args.blockchainId,
				name			= args.name,
				symbol			= args.symbol,
				standard		= args.standard,
				address			= args.address,
				decimals		= args.decimals,
				image			= args.image,
				hidden			= args.hidden
			)

	def update(self, args):
		self.blockchainId 		= args.blockchainId
		self.name				= args.name,
		self.symbol				= args.symbol,
		self.standard			= args.standard,
		self.address			= args.address,
		self.decimals			= args.decimals,
		self.image				= args.image,
		self.hidden				= args.hidden

	def patch(self, args):
		if args.blockchainId != None:
			self.blockchainId 	= args.blockchainId

		if args.name != None:
			self.name 			= args.name

		if args.symbol != None:
			self.symbol 		= args.symbol

		if args.standard != None:
			self.standard 		= args.standard

		if args.address != None:
			self.address 		= args.address

		if args.decimals != None:
			self.decimals 		= args.decimals

		if args.image != None:
			self.image 			= args.image

		if args.hidden != None:
			self.hidden 		= args.hidden
