from flask_restful import reqparse
from sqlalchemy_serializer import SerializerMixin

from app.models import db
from app.doc import swagger


parser = reqparse.RequestParser()
parser.add_argument(
	'chainId',
	type=int,
	help='ID of the Blockchain.'
)

parser.add_argument(
	'chainName',
	type=str,
	help='Name of the Blockchain.'
)

parser.add_argument(
	'currencyName',
	type=str,
	help='Currency name used by the Blockchain.'
)

parser.add_argument(
	'currencySymbol',
	type=str,
	help='Symbol of the currency used by the Blockchain.'
)

parser.add_argument(
	'decimals',
	type=int,
	help='Decimals places of the courrency used by the Blockchain.'
)

@swagger.definition('Blockchain')
class Blockchain(db.Model, SerializerMixin):

	# Swagger
	"""
	Blockchain Object inorder to  repressent a blockchain.
	---
	type: object
	properties:
		id:
			type: integer
			description: ID of the blockchain in local database.
		chainId:
			type: integer
			description: Global recognized ID of blockchain.
		chainName:
			type: string
			description: Name of the blockchain.
		currencyName:
			type: string
			description: Native used currency name.
		currencySymbol:
			type: string
			description: Native used currency symbol.
		decimals:
			type: integer
			description: Decimal places of the native currency.
	"""


	
	serialize_only = (
		'id',
		'chainId',
		'chainName',
		'currencyName',
		'currencySymbol',
		'decimals'
	)

	id = db.Column(
		db.Integer,
		primary_key=True
	)

	chainId = db.Column(
		db.Integer,
		unique=True,
		nullable=False
	)
	
	chainName = db.Column(
		db.String(255),
		unique=True,
		nullable=False
	)

	currencyName = db.Column(
		db.String(255),
		nullable=False
	)

	currencySymbol = db.Column(
		db.String(40),
		nullable=False
	)

	decimals = db.Column(
		db.Integer,
		nullable=False
	)

	rpcs = db.relationship(
		'RPC',
		backref='blockchain',
		order_by="RPC.order",
		lazy=True
	)

	explorers = db.relationship(
		'Explorer',
		backref='blockchain',
		order_by="Explorer.order",
		lazy=True
	)

	tokens = db.relationship(
		'Token',
		backref='blockchain',
		order_by="Token.name",
		lazy=True
	)

	swaps = db.relationship(
		'SWAP',
		backref='blockchain',
		order_by="SWAP.order",
		lazy=True
	)

	def __repr__(self):
		return "<Blockchain %r>" % (self.id)

	def __init__(self, args=None, **kwargs):
		if args == None:
			super(Blockchain, self).__init__(**kwargs)
		else:
			super(Blockchain, self).__init__(
				chainId=args.chainId,
				chainName=args.chainName,
				currencyName=args.currencyName,
				currencySymbol=args.currencySymbol,
				decimals=args.decimals
			)

	def update(self, args):
		self.chainId 		= args.chainId
		self.chainName 		= args.chainName
		self.currencyName 	= args.currencyName
		self.currencySymbol = args.currencySymbol
		self.decimals 		= args.decimals

	def patch(self, args):
		if(args.chainId != None):
			self.chainId = args.chainId

		if(args.chainName != None):
			self.chainName = args.chainName

		if(args.currencyName != None):
			self.currencyName = args.currencyName

		if(args.currencySymbol != None):
			self.currencySymbol = args.currencySymbol

		if(args.decimals != None):
			self.decimals = args.decimals

	def getTokenByAddress(self, address):
		ftokens = list(
			filter(
				lambda token: token.address.lower() == address.lower(),
				self.tokens
			)
		)
		return next(iter(ftokens), None)
