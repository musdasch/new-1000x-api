from flask_restful import reqparse
from sqlalchemy_serializer import SerializerMixin

from app.models import db
from app.doc import swagger


parser = reqparse.RequestParser()

parser.add_argument(
	'blockchainId',
	type=int,
	help='ID of the Blockchain.'
)

parser.add_argument(
	'url',
	type=str,
	help='URL of the RPC interface.'
)

parser.add_argument(
	'order',
	type=int,
	help='Order of the explorer.'
)


@swagger.definition('Explorer')
class Explorer(db.Model, SerializerMixin):

	# Swagger
	"""
	Explorer Object
	---
	type: object
	properties:
		id:
			type: integer
			description: ID of the explorer.
		url:
			type: string
			description: URL of the explorer.
		order:
			type: integer
			description: Order of the explorer
		blockchainId:
			type: integer
			description: ID of the Blockchain as in the local database.
	"""
	
	serialize_only = (
		'id',
		'url',
		'order',
		'blockchainId'
	)

	id = db.Column(
		db.Integer,
		primary_key=True
	)

	url = db.Column(
		db.String(255),
		unique=True,
		nullable=False
	)

	order = db.Column(
		db.Integer,
		nullable=False
	)

	blockchainId = db.Column(
		db.Integer,
		db.ForeignKey('blockchain.id'),
		nullable=False
	)

	def __repr__(self):
		return "<Explorer %r>" % (self.id)

	def __init__(self, args=None, **kwargs):
		if args==None:
			super(Explorer, self).__init__(**kwargs)
		else:
			super(Explorer, self).__init__(
				blockchainId	= args.blockchainId,
				url				= args.url,
				order			= args.order
			)

	def update(self, args):
		self.blockchainId 		= args.blockchainId
		self.url 				= args.url
		self.order 				= args.order

	def patch(self, args):
		if args.blockchainId != None:
			self.blockchainId 	= args.blockchainId

		if args.url != None:
			self.url 			= args.url

		if args.order != None:
			self.order 			= args.order
