from flask_restful import reqparse
from sqlalchemy_serializer import SerializerMixin


from app.models import db
from app.doc import swagger


parser = reqparse.RequestParser()

@swagger.definition('Trade')
class Trade(db.Model, SerializerMixin):
	
	# Swagger
	"""
	Token Object
	---
	type: object
	properties:
		id:
			type: integer
			description: ID of the trade.
		swapId:
			type: integer
			description: ID of the swap on which the trade has happened.
		date:
			type: datetime
			description: Date on which the trade has happened.
		blockNumber:
			type: integer
			description: The block number in which the trade has been approved.
		transactionHash:
			type: string
			description: The transaction hash of the trade.
		sender:
			type: string
			description: Address of the wallet who sent the transaction.
		tokenInId:
			type: integer
			description: ID of the token with which has been paid.
		tokenOutId:
			type: integer
			description: ID of the token which has been received by the sender.
		amountIn:
			type: float
			description: Amount how has been sent by the sender.
		amountOut:
			type: float
			description: Amount how has been received by the sender.
	"""

	serialize_only = (
		'id',
		'swapId',
		'date',
		'blockNumber',
		'transactionHash',
		'sender',
		'tokenInId',
		'tokenOutId',
		'amountIn',
		'amountOut'
	)

	id = db.Column(
		db.Integer,
		primary_key=True
	)

	swapId = db.Column(
		db.Integer,
		db.ForeignKey('swap.id'),
		nullable=False
	)

	date = db.Column(
		db.DateTime,
		unique=False,
		nullable=False
	)

	blockNumber = db.Column(
		db.Integer,
		unique=False,
		nullable=False
	)

	transactionHash = db.Column(
		db.String(66),
		unique=True,
		nullable=False
	)

	sender = db.Column(
		db.String(42),
		unique=False,
		nullable=False
	)

	tokenInId = db.Column(
		db.Integer,
		db.ForeignKey('token.id'),
		nullable=False
	)
	
	tokenOutId = db.Column(
		db.Integer,
		db.ForeignKey('token.id'),
		nullable=False
	)

	amountIn = db.Column(
		db.Float,
		unique=False,
		nullable=False
	)

	amountOut = db.Column(
		db.Float,
		unique=False,
		nullable=False
	)

	def __repr__(self):
		return "<Trade %r>" % (self.id)

	def __init__(self, args=None, **kwargs):
		if args==None:
			super(Trade, self).__init__(**kwargs)
		else:
			super(Trade, self).__init__(
				swapId				= args.swapId,
				date				= args.date,
				blockNumber			= args.blockNumber,
				transactionHash		= args.transactionHash,
				sender				= args.sender,
				tokenInId			= args.tokenInId,
				tokenOutId			= args.tokenOutId,
				amountIn			= args.amountIn,
				amountOut			= args.amountOut
			)

	def update(self, args):
		self.swapId					= args.swapId
		self.date					= args.date
		self.blockNumber			= args.blockNumber
		self.transactionHash		= args.transactionHash
		self.sender					= args.sender
		self.tokenInId				= args.tokenInId
		self.tokenOutId				= args.tokenOutId
		self.amountIn				= args.amountIn
		self.amountOut				= args.amountOut

	def patch(self, args):
		if args.swapId != None:
			self.swapId 				= args.swapId
		
		if args.date != None:
			self.date 				= args.date

		if args.blockNumber != None:
			self.blockNumber 		= args.blockNumber

		if args.transactionHash != None:
			self.transactionHash 	= args.transactionHash

		if args.sender != None:
			self.sender 			= args.sender

		if args.tokenOutId != None:
			self.tokenOutId 		= args.tokenOutId

		if args.amountIn != None:
			self.amountIn 			= args.amountIn

		if args.amountOut != None:
			self.amountOut 			= args.amountOut
