from flask_restful import reqparse
from sqlalchemy_serializer import SerializerMixin

from app.models import db
from app.doc import swagger

parser = reqparse.RequestParser()

parser.add_argument(
	'blockchainId',
	type=int,
	help='ID of the Blockchain.'
)

parser.add_argument(
	'name',
	type=str,
	help='name of the swap.'
)

parser.add_argument(
	'url',
	type=str,
	help='URL of the swap page.'
)

parser.add_argument(
	'address',
	type=str,
	help='Address the contract is deployed at.'
)

parser.add_argument(
	'abi',
	type=str,
	help='ABI json file'
)

parser.add_argument(
	'trackHistory',
	type=bool,
	help='sould the trading history be tracked.'
)

parser.add_argument(
	'lastCheckedBlock',
	type=int,
	help='The last checked block for trades.'
)

parser.add_argument(
	'order',
	type=int,
	help='Order of the swap.'
)


# Association Tabele
swap_token = db.Table('swap_token',
	db.Column(
		'swap_id',
		db.Integer,
		db.ForeignKey('swap.id')
	),
	db.Column(
		'token_id',
		db.Integer,
		db.ForeignKey('token.id')
	)
)


@swagger.definition('SWAP')
class SWAP(db.Model, SerializerMixin):

	# Swagger
	"""
	SWAP Object
	---
	type: object
	properties:
		id:
			type: integer
			description: ID of the swap.
		name:
			type: string
			description: Name of the swap.
		url:
			type: string
			description: URL of the UI.
		address:
			type: string
			description: Address of the swap contract.
		abi:
			type: string
			description: Name of the abi file.
		trackHistory:
			type: bool
			description: If the api tracks the trading history.
		lastCheckedBlock:
			type: integer
			description: Trading history tracked up to block.
		order:
			type: integer
			description: Order of the swap.
		blockchainId:
			type: integer
			description: Local ID of the blockchain.
	"""
	
	serialize_only = (
		'id',
		'name',
		'url',
		'address',
		'abi',
		'trackHistory',
		'lastCheckedBlock',
		'order',
		'blockchainId'
	)

	id = db.Column(
		db.Integer,
		primary_key=True
	)

	blockchainId = db.Column(
		db.Integer,
		db.ForeignKey('blockchain.id'),
		nullable=False
	)

	name = db.Column(
		db.String(255),
		unique=True,
		nullable=False
	)

	url = db.Column(
		db.String(255),
		unique=True,
		nullable=False
	)

	address = db.Column(
		db.String(42),
		nullable=True
	)

	abi = db.Column(
		db.String(255),
		unique=True,
		nullable=False
	)

	tokens = db.relationship(
		'Token',
		secondary=swap_token,
		order_by="Token.name",
		backref='swaps'
	)

	trackHistory = db.Column(
		db.Boolean,
		nullable=False
	)

	lastCheckedBlock = db.Column(
		db.Integer,
		nullable=False
	)

	order = db.Column(
		db.Integer,
		nullable=False
	)

	trades = db.relationship(
		'Trade',
		backref='swap',
		order_by="Trade.date",
		lazy=True
	)


	def __repr__(self):
		return "<SWAP %r>" % (self.id)

	def __init__(self, args=None, **kwargs):
		if args==None:
			super(SWAP, self).__init__(**kwargs)
		else:
			super(SWAP, self).__init__(
				blockchainId		= args.blockchainId,
				name				= args.name,
				url					= args.url,
				address				= args.address,
				abi					= args.abi,
				trackHistory		= args.trackHistory,
				lastCheckedBlock	= args.lastCheckedBlock,
				order				= args.order
			)

	def update(self, args):
		self.blockchainId 			= args.blockchainId
		self.name 					= args.name
		self.url 					= args.url
		self.address 				= args.address
		self.abi 					= args.abi
		self.trackHistory 			= args.trackHistory
		self.lastCheckedBlock 		= args.lastCheckedBlock
		self.order 					= args.order

	def patch(self, args):
		if args.blockchainId != None:
			self.blockchainId 		= args.blockchainId

		if args.name != None:
			self.name 				= args.name

		if args.url != None:
			self.url 				= args.url

		if args.address != None:
			self.address 			= args.address

		if args.abi != None:
			self.abi 				= args.abi

		if args.trackHistory != None:
			self.trackHistory 		= args.trackHistory

		if args.lastCheckedBlock != None:
			self.lastCheckedBlock 	= args.lastCheckedBlock

		if args.order != None:
			self.order 				= args.order
