from flask_restful import reqparse
from sqlalchemy_serializer import SerializerMixin
from werkzeug.security import generate_password_hash, check_password_hash


from app.models import db


parser = reqparse.RequestParser()
parser.add_argument(
	'name',
	type=str,
	help='Name of the new user.'
)

parser.add_argument(
	'email',
	type=str,
	help='E-Mail of the new user.'
)

parser.add_argument(
	'level',
	type=int,
	help='Permission level of the user.'
)

parser.add_argument(
	'password',
	type=str,
	help='Password of the new user.'
)

class User(db.Model, SerializerMixin):
	
	serialize_only = ('id', 'name', 'email', 'level')

	id = db.Column(
		db.Integer,
		primary_key=True
	)
	
	name = db.Column(
		db.String(40),
		unique=True,
		nullable=False
	)

	password_hash = db.Column(
		db.String(88)
	)

	email = db.Column(
		db.String(40),
		unique=True
	)

	level = db.Column(
		db.Integer,
		nullable=False
	)

	def set_password(self, password):
		self.password_hash = generate_password_hash(
			password,
			method='sha256'
		)

	def check_password(self, password):
		return check_password_hash(self.password_hash, password)

	def __repr__(self):
		return "<User %r>" % (self.id)

	def __init__(self, args=None, **kwargs):
		if args == None:
			super(User, self).__init__(**kwargs)
		else:
			super(User, self).__init__(
				name=args.name,
				email=args.email,
				level=args.level
			)

			self.set_password(args.password)

	def update(self, args):
		self.name = args.name
		self.email = args.email
		self.level = args.level
		self.set_password(args.password)

	def patch(self, args):
		if(args.name != None):
			self.name = args.name

		if(args.email != None):
			self.email = args.email

		if(args.level != None):
			self.level = args.level

		if(args.password != None):
			self.set_password(args.password)
