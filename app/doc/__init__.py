import re

from flask import request
from flasgger import Swagger
from flask_jwt_extended import verify_jwt_in_request

swagger_config = Swagger.DEFAULT_CONFIG
#swagger_config['swagger_ui_css'] = '/static/css/swagger/theme-material.css'

swagger = Swagger(config=swagger_config);

def init_app(app):
	swagger.init_app(app)
	
	@app.before_request
	def before_request():
		verify_jwt_in_request(optional=True)