import os
from apscheduler.schedulers.background import BackgroundScheduler

from .trade_cron import fetchTradesCron

scheduler = BackgroundScheduler()

def initInterval(app):
	if app.config['RUN_CRON_JOBS']:
		if not app.debug or os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
			scheduler.add_job(
				fetchTradesCron,
				"cron",
				[app],
				minute 	= app.config['RUN_CRON_TRADE_MINUTE']
			)
			
			scheduler.start()
