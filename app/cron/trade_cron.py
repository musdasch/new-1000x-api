import time, atexit

from web3 import Web3, EthereumTesterProvider
from datetime import datetime

from app.models import db
from app.models.blockchain import Blockchain
from app.models.token import Token
from app.models.trade import Trade

def getSWAPABI(file):
	with open("data/abis/" + file, "r") as abi:
		return abi.read()

def getWeb3(blockchain):
	providers = list(map(lambda rpc: rpc.url, blockchain.rpcs))
	return Web3(Web3.HTTPProvider(providers[0]))

def fetchTrades(blockchain, swap, w3):
	if w3.is_connected():
		address 	= swap.address
		fromBlock 	= swap.lastCheckedBlock + 1
		toBlock		= w3.eth.get_block('latest').number - 5
		abi 		= getSWAPABI(swap.abi)

		swapContract = w3.eth.contract(address=address, abi=abi)

		events = swapContract.events.newSWAP.create_filter(
			fromBlock 	= fromBlock,
			toBlock 	= toBlock
		).get_all_entries()

		for event in events:
			timestamp = w3.eth.get_block(event.blockNumber).timestamp
			date 		= datetime.fromtimestamp(timestamp)
			tokenIn 	= blockchain.getTokenByAddress(event.args.tokenIn)
			tokenOut 	= blockchain.getTokenByAddress(event.args.tokenOut)
			amountIn 	= event.args.amountIn/pow(10, tokenIn.decimals)
			amountOut 	= event.args.amountOut/pow(10, tokenOut.decimals)

			item = Trade(
				swapId				= swap.id,
				date				= date,
				blockNumber			= event.blockNumber,
				transactionHash		= event.transactionHash.hex(),
				sender				= event.args.sender,
				tokenInId			= tokenIn.id,
				tokenOutId			= tokenOut.id,
				amountIn			= amountIn,
				amountOut			= amountOut
			)

			db.session.add(item)
		
		swap.lastCheckedBlock = toBlock
		db.session.commit()

def fetchTradesCron(app):
	with app.app_context():
		blockchains = Blockchain().query.all()
		for blockchain in blockchains:
			w3 = getWeb3(blockchain)

			for swap in blockchain.swaps:
				if swap.trackHistory:
					fetchTrades(blockchain, swap, w3)
