from flask_restful import Resource

from app.models import db
from app.models.trade import Trade, parser

class TradeResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get Trade
		Returns an Trade by ID which has been made on the 1000x swap.
		
		---
		tags:
			- Trade
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends a JSON object of Trade data.
			'404':
				description: Trade not found. Sends a JSON object of error data.
		"""

		item = Trade().query.filter_by(id=id).first_or_404()
		return item.to_dict()

class TradesResource(Resource):
	def get(self):
		# Swagger
		"""
		Get Trades
		Returns all Trades as an array which have been made on the 1000x swap.
		
		---
		tags:
			- Trade

		responses:
			'200':
				description: Ok. Sends a JSON array of Trade data.
			'404':
				description: Trade not found. Sends a JSON object of error data.
		"""

		items = Trade().query.order_by(Trade.blockNumber.desc()).all()
		return list(map(lambda item: item.to_dict(), items))
