from flask_restful import Resource

from app.doc import swagger
from app.models import db
from app.models.blockchain import Blockchain, parser
from app.auth.authorization import userIsAdmin


class BlockchainResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get blockchain
		Returns a blockchain by ID.
		
		---
		tags:
			- Blockchain
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends a JSON object of blockchain data.
			'404':
				description: Blockchain not found. Sends a JSON object of error data.
		"""

		item = Blockchain().query.filter_by(id=id).first_or_404()
		return item.to_dict()

	@userIsAdmin()
	def put(self, id):
		args = parser.parse_args()
		
		item = Blockchain().query.filter_by(id=id).first_or_404()
		item.update(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating blockchain'}, 400

	@userIsAdmin()
	def patch(self, id):
		args = parser.parse_args()

		item = Blockchain().query.filter_by(id=id).first_or_404()
		item.patch(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating blockchain'}, 400

	@userIsAdmin()
	def delete(self, id):
		try:
			db.session.delete(
				Blockchain().query.filter_by(id=id).first_or_404()
			)
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting blockchain'}, 400


class BlockchainByChainIdResource(Resource):
	def get(self, chainId):
		# Swagger
		"""
		Get blockchain by chain ID
		Returns a blockchain by ID.
		
		---
		tags:
			- Blockchain
		parameters:
			- name: chainId
			  in: path

		responses:
			'200':
				description: Ok. Sends a JSON object of blockchain data.
			'404':
				description: Blockchain not found. Sends a JSON object of error data.

		"""

		item = Blockchain().query.filter_by(chainId=chainId).first_or_404()
		return item.to_dict()

class BlockchainsResource(Resource):
	def get(self):
		
		# Swagger
		"""
		Get all blockchains
		Returns a blockchain by ID.
		
		---
		tags:
			- Blockchain
		responses:
			'200':
				description: Ok. Sends a JSON Array with blockchain objects.

		"""

		items = Blockchain().query.all()
		return list(map(lambda item: item.to_dict(), items))

	@userIsAdmin()
	def post(self):
		item = Blockchain(
			parser.parse_args()
		)

		db.session.add(item)

		try:
			db.session.commit()
			return item.to_dict(), 201
		
		except:
			return {'error': 'Error creating blockchain'}, 400

class BlockchainRPCsResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get RPCs of a blockchain.
		Returns all RPCs of a blockchain by ID.
		
		---
		tags:
			- Blockchain
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends an array with RPCs for a given blockchain. 
			'404':
				description: Blockchain not found. Sends a JSON object of error data.
		"""

		blockchain = Blockchain().query.filter_by(id=id).first_or_404()
		return list(map(lambda item: item.to_dict(), blockchain.rpcs))

class BlockchainExplorersResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get explorers of a blockchain.
		Returns all explorers of a blockchain by ID.
		
		---
		tags:
			- Blockchain
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends an array with explorers for a given blockchain. 
			'404':
				description: Blockchain not found. Sends a JSON object of error data.
		"""

		blockchain = Blockchain().query.filter_by(id=id).first_or_404()
		return list(map(lambda item: item.to_dict(), blockchain.explorers))

class BlockchainSWAPsResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get swaps of a blockchain.
		Returns all swaps of a blockchain by ID.
		
		---
		tags:
			- Blockchain
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends an array with swaps for a given blockchain. 
			'404':
				description: Blockchain not found. Sends a JSON object of error data.
		"""

		blockchain = Blockchain().query.filter_by(id=id).first_or_404()
		return list(map(lambda item: item.to_dict(), blockchain.swaps))

class BlockchainTokensResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get tokens of a blockchain.
		Returns all tokens of a blockchain by ID.
		
		---
		tags:
			- Blockchain
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends an array with tokens for a given blockchain. 
			'404':
				description: Blockchain not found. Sends a JSON object of error data.
		"""

		blockchain = Blockchain().query.filter_by(id=id).first_or_404()
		return list(map(lambda item: item.to_dict(), blockchain.tokens))
