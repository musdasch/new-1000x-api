from flask_restful import Resource

from app.models import db
from app.models.user import User, parser
from app.auth.authorization import userIsAdmin

class UserResource(Resource):

	@userIsAdmin()
	def get(self, id):
		item = User().query.filter_by(id=id).first_or_404()
		return item.to_dict()

	@userIsAdmin()
	def put(self, id):
		args = parser.parse_args()
		
		item = User().query.filter_by(id=id).first_or_404()
		item.update(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating user'}, 400

	@userIsAdmin()
	def patch(self, id):
		args = parser.parse_args()
		
		item = User().query.filter_by(id=id).first_or_404()
		item.patch(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating user'}, 400

	@userIsAdmin()
	def delete(self, id):
		item = User().query.filter_by(id=id).first_or_404()

		try:
			db.session.delete(item)
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting user'}, 400


class UsersResource(Resource):
	@userIsAdmin()
	def get(self):
		items = User().query.all()
		return list(map(lambda item: item.to_dict(), items))

	@userIsAdmin()
	def post(self):
		args = parser.parse_args()

		item = User(args)
		db.session.add(item)

		try:
			db.session.commit()
			return item.to_dict(), 201
		
		except:
			return {'error': 'Error creating user'}, 400
