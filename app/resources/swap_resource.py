from flask_restful import Resource

from app.models import db
from app.models.swap import SWAP, parser
from app.models.token import Token
from app.auth.authorization import userIsAdmin

class SWAPResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get SWAP
		Returns an SWAP by ID.
		
		---
		tags:
			- SWAP
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends a JSON object of SWAP data.
			'404':
				description: SWAP not found. Sends a JSON object of error data.
		"""

		item = SWAP().query.filter_by(id=id).first_or_404()
		return item.to_dict()

	@userIsAdmin()
	def put(self, id):
		args = parser.parse_args()
		
		item = SWAP().query.filter_by(id=id).first_or_404()
		item.update(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating swap'}, 400

	@userIsAdmin()
	def patch(self, id):
		args = parser.parse_args()
		
		item = SWAP().query.filter_by(id=id).first_or_404()
		item.patch(args)
		
		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating swap'}, 400

	@userIsAdmin()
	def delete(self, id):
		item = SWAP().query.filter_by(id=id).first_or_404()

		try:
			db.session.delete(item)
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting swap'}, 400


class SWAPsResource(Resource):
	def get(self):
		# Swagger
		"""
		Get SWAPs
		Returns all SWAPs as an array.
		
		---
		tags:
			- SWAP

		responses:
			'200':
				description: Ok. Sends a JSON array of SWAP data.
			'404':
				description: SWAP not found. Sends a JSON object of error data.
		"""

		items = SWAP().query.all()
		return list(map(lambda item: item.to_dict(), items))

	@userIsAdmin()
	def post(self):
		args = parser.parse_args()

		item = SWAP(args)
		db.session.add(item)

		try:
			db.session.commit()
			return item.to_dict(), 201
		
		except:
			return {'error': 'Error creating swap'}, 400

class SWAPTokensResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get tokens of a SWAP.
		Returns all tokens of a SWAP by ID.
		
		---
		tags:
			- SWAP
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends an array with tokens for a given SWAP. 
			'404':
				description: SWAP not found. Sends a JSON object of error data.
		"""

		item = SWAP().query.filter_by(id=id).first_or_404()
		items = list(map(lambda item: item.to_dict(), item.tokens))

		return items

class SWAPTokenResource(Resource):
	@userIsAdmin()
	def post(self, swapId, tokenId):
		swap = SWAP().query.filter_by(id=swapId).first_or_404()
		token = Token().query.filter_by(id=tokenId).first_or_404()

		swap.tokens.append(token)

		try:
			db.session.commit()
			return token.to_dict(), 201
		
		except:
			return {'error': 'Error adding token to swap'}, 400

	@userIsAdmin()
	def delete(self, swapId, tokenId):
		swap = SWAP().query.filter_by(id=swapId).first_or_404()
		token = Token().query.filter_by(id=tokenId).first_or_404()

		swap.tokens.remove(token)

		try:
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting swap'}, 400
