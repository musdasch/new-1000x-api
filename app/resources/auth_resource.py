from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token

from app.models import db
from app.models.user import User

parser = reqparse.RequestParser()
parser.add_argument('name', type=str, help='Name of the new user.')
parser.add_argument('password', type=str, help='password of the new user.')

class AuthResource(Resource):
	def post(self):
		args = parser.parse_args()
		_user = User().query.filter_by(name=args.name).first()
	
		if _user and _user.check_password(args.password):
			access_token = create_access_token(identity=_user)
			return {'access_token': access_token}, 200

		return {'message': 'Forbidden'}, 403