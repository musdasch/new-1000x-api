from flask_restful import Api
from flask_cors import CORS


from .auth_resource import AuthResource
from .user_resource import UserResource, UsersResource
from .blockchain_resource import BlockchainResource, BlockchainsResource, BlockchainByChainIdResource, BlockchainRPCsResource, BlockchainExplorersResource, BlockchainSWAPsResource, BlockchainTokensResource
from .rpc_resource import RPCResource, RPCsResource
from .swap_resource import SWAPResource, SWAPsResource, SWAPTokensResource, SWAPTokenResource
from .explorer_resource import ExplorerResource, ExplorersResource
from .token_resource import TokenResource, TokensResource
from .trade_resource import TradeResource, TradesResource

api = Api()

def init_app(app):
	api.add_resource(AuthResource, 							'/api/auth/')
	
	api.add_resource(UserResource, 							'/api/user/<int:id>')
	api.add_resource(UsersResource, 						'/api/user/')

	api.add_resource(BlockchainExplorersResource, 			'/api/blockchain/<int:id>/explorer/' )
	api.add_resource(BlockchainRPCsResource, 				'/api/blockchain/<int:id>/rpc/' )
	api.add_resource(BlockchainTokensResource, 				'/api/blockchain/<int:id>/token/' )
	api.add_resource(BlockchainSWAPsResource, 				'/api/blockchain/<int:id>/swap/' )
	api.add_resource(BlockchainByChainIdResource, 			'/api/blockchain/by_chain_id/<int:chainId>' )
	api.add_resource(BlockchainResource, 					'/api/blockchain/<int:id>' )
	api.add_resource(BlockchainsResource, 					'/api/blockchain/' )

	api.add_resource(RPCResource, 							'/api/rpc/<int:id>')
	api.add_resource(RPCsResource, 							'/api/rpc/')

	api.add_resource(ExplorerResource, 						'/api/explorer/<int:id>')
	api.add_resource(ExplorersResource, 					'/api/explorer/')

	api.add_resource(TokenResource, 						'/api/token/<int:id>')
	api.add_resource(TokensResource, 						'/api/token/')

	api.add_resource(TradeResource, 						'/api/trade/<int:id>')
	api.add_resource(TradesResource, 						'/api/trade/')

	api.add_resource(SWAPTokenResource, 					'/api/swap/<int:swapId>/token/<int:tokenId>')
	api.add_resource(SWAPTokensResource, 					'/api/swap/<int:id>/token/')
	api.add_resource(SWAPResource, 							'/api/swap/<int:id>')
	api.add_resource(SWAPsResource, 						'/api/swap/')

	api.init_app(app)
	CORS(app, resources={r"/api/*": {"origins": "*"}})
