from flask_restful import Resource

from app.models import db
from app.models.rpc import RPC, parser
from app.auth.authorization import userIsAdmin

class RPCResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get RPC
		Returns an RPC by ID.
		
		---
		tags:
			- RPC
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends a JSON object of RPC data.
			'404':
				description: RPC not found. Sends a JSON object of error data.
		"""

		item = RPC().query.filter_by(id=id).first_or_404()
		return item.to_dict()

	@userIsAdmin()
	def put(self, id):
		args = parser.parse_args()
		
		item = RPC().query.filter_by(id=id).first_or_404()
		item.update(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating rpc'}, 400

	@userIsAdmin()
	def patch(self, id):
		args = parser.parse_args()
		
		item = RPC().query.filter_by(id=id).first_or_404()
		item.patch(args)
		
		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating rpc'}, 400

	@userIsAdmin()
	def delete(self, id):
		item = RPC().query.filter_by(id=id).first_or_404()

		try:
			db.session.delete(item)
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting rpc'}, 400


class RPCsResource(Resource):
	def get(self):
		# Swagger
		"""
		Get RPCs
		Returns all RPCs as an array.
		
		---
		tags:
			- RPC

		responses:
			'200':
				description: Ok. Sends a JSON array of RPC data.
			'404':
				description: RPC not found. Sends a JSON object of error data.
		"""
		
		items = RPC().query.order_by(RPC.order.asc()).all()
		return list(map(lambda item: item.to_dict(), items))

	@userIsAdmin()
	def post(self):
		args = parser.parse_args()

		item = RPC(args)
		db.session.add(item)

		try:
			db.session.commit()
			return item.to_dict(), 201
		
		except:
			return {'error': 'Error creating rpc'}, 400
