from flask_restful import Resource

from app.models import db
from app.models.explorer import Explorer, parser
from app.auth.authorization import userIsAdmin

class ExplorerResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get explorer
		Returns an explorer by ID.
		
		---
		tags:
			- Explorer
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends a JSON object of explorer data.
			'404':
				description: Blockchain not found. Sends a JSON object of error data.
		"""

		item = Explorer().query.filter_by(id=id).first_or_404()
		return item.to_dict()

	@userIsAdmin()
	def put(self, id):
		args = parser.parse_args()
		
		item = Explorer().query.filter_by(id=id).first_or_404()
		item.update(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating explorer'}, 400

	@userIsAdmin()
	def patch(self, id):
		args = parser.parse_args()
		
		item = Explorer().query.filter_by(id=id).first_or_404()
		item.patch(args)
		
		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating explorer'}, 400

	@userIsAdmin()
	def delete(self, id):
		item = Explorer().query.filter_by(id=id).first_or_404()

		try:
			db.session.delete(item)
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting explorer'}, 400


class ExplorersResource(Resource):
	def get(self):
		# Swagger
		"""
		Get explorer
		Returns all explorer.
		
		---
		tags:
			- Explorer
		responses:
			'200':
				description: Ok. Sends an array of explorer data.
			'404':
				description: Blockchain not found. Sends a JSON object of error data.
		"""

		items = Explorer().query.all()
		return list(map(lambda item: item.to_dict(), items))

	@userIsAdmin()
	def post(self):
		args = parser.parse_args()

		item = Explorer(args)
		db.session.add(item)

		try:
			db.session.commit()
			return item.to_dict(), 201
		
		except:
			return {'error': 'Error creating explorer'}, 400
