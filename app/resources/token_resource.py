from flask_restful import Resource

from app.models import db
from app.models.token import Token, parser
from app.auth.authorization import userIsAdmin

class TokenResource(Resource):
	def get(self, id):
		# Swagger
		"""
		Get Token
		Returns an Token by ID.
		
		---
		tags:
			- Token
		parameters:
			- name: id
			  in: path

		responses:
			'200':
				description: Ok. Sends a JSON object of Token data.
			'404':
				description: Token not found. Sends a JSON object of error data.
		"""

		item = Token().query.filter_by(id=id).first_or_404()
		return item.to_dict()

	@userIsAdmin()
	def put(self, id):
		args = parser.parse_args()
		
		item = Token().query.filter_by(id=id).first_or_404()
		item.update(args)

		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating token'}, 400

	@userIsAdmin()
	def patch(self, id):
		args = parser.parse_args()
		
		item = Token().query.filter_by(id=id).first_or_404()
		item.patch(args)
		
		try:
			db.session.commit()
			return item.to_dict(), 200
		
		except:
			return {'error': 'Error updating token'}, 400

	@userIsAdmin()
	def delete(self, id):
		item = Token().query.filter_by(id=id).first_or_404()

		try:
			db.session.delete(item)
			db.session.commit()
			return [], 204
		
		except:
			return {'error': 'Error deleting token'}, 400


class TokensResource(Resource):
	def get(self):
		# Swagger
		"""
		Get Tokens
		Returns all Tokens as an array.
		
		---
		tags:
			- Token

		responses:
			'200':
				description: Ok. Sends a JSON array of Token data.
			'404':
				description: Token not found. Sends a JSON object of error data.
		"""

		items = Token().query.all()
		return list(map(lambda item: item.to_dict(), items))

	@userIsAdmin()
	def post(self):
		args = parser.parse_args()

		item = Token(args)
		db.session.add(item)

		try:
			db.session.commit()
			return item.to_dict(), 201
		
		except:
			return {'error': 'Error creating token'}, 400
