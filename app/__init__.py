from flask import Flask
from . import auth, commands, models, resources, cron, doc

def create_app():
	app = Flask(__name__)
	app.config.from_object('config.Config')
	
	doc.init_app(app)
	models.init_app(app)
	auth.init_app(app)
	resources.init_app(app)
	commands.init_app(app)

	cron.initInterval(app)

	return app
