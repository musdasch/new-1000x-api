from flask import current_app
from flask_jwt_extended import jwt_required, current_user

def userLevelGreaterOrEquals(level):
	def decorator(func):
		@jwt_required()
		def wrapper(*args, **kwargs):
			if current_user.level >= level:
				return func(*args, **kwargs)
			else:
				return {'error': 'User is not allowed to execute this request.'}, 401

		return wrapper
	return decorator

def userIsAdmin():
	def decorator(func):
		@jwt_required()
		def wrapper(*args, **kwargs):
			if current_user.level >= current_app.config['ADMIN_LEVEL']:
				return func(*args, **kwargs)
			else:
				return {'error': 'User is not allowed to execute this request.'}, 401

		return wrapper
	return decorator