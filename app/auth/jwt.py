from flask import redirect, url_for
from flask_jwt_extended import JWTManager

from app.models.user import User

jwt = JWTManager()

@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.id

@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User().query.filter_by(id=identity).one_or_none()

@jwt.expired_token_loader
def expired_token_callback(_jwt_header, jwt_data):
    return redirect(url_for('auth.logout'))

def init_app(app):
	jwt.init_app(app, True)